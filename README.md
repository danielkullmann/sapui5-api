# UI5 API Docs

This is supposed to be a faster alternative to https://sapui5.hana.ondemand.com and https://openui5.hana.ondemand.com.

## Resources:

### OpenUI5

* List of versions: https://openui5.hana.ondemand.com/versionoverview.json
* The api docs for a specific version can then be retrieved from a base URL like https://openui5.hana.ondemand.com/1.87.0/
* List of available packages and classes: https://openui5.hana.ondemand.com/docs/api/api-index.json
* There are multiple api.json files for different parts of the API, e.g. https://openui5.hana.ondemand.com/test-resources/sap/m/designtime/apiref/api.json and https://openui5.hana.ondemand.com/test-resources/sap/ui/core/designtime/apiref/api.json

### SAPUI5

* List of versions: https://sapui5.hana.ondemand.com/versionoverview.json
* List of available packages and classes: https://sapui5.hana.ondemand.com/docs/api/api-index.json


