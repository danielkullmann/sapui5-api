#!/usr/bin/python3
"""
Downloads all needed data from the sapui5 and openui5 API doc sites.
"""

import json
import os
import urllib.request

OPENUI5 = "openui5"
SAPUI5 = "sapui5"

def read_url_as_text(url):
    req = urllib.request.Request(url)
    with urllib.request.urlopen(req) as fh:
        return fh.read().decode("utf-8")

#def read_url_as_json(url):
#    req = urllib.request.Request(url)
#    with urllib.request.urlopen(req) as fh:
#        return json.load(fh)

def baseUrl(key):
    return "https://" + key + ".hana.ondemand.com/"

def read_api_index(key):
    print("Download api index", key)
    url = baseUrl(key) + "/docs/api/api-index.json"
    content = read_url_as_text(url)
    return json.loads(content)

def read_library(key, library):
    print("Download library", key, library)
    url = baseUrl(key) + "test-resources/" + library.replace(".", "/") + "/designtime/apiref/api.json"
    content = read_url_as_text(url)
    return json.loads(content)

def recurse_api_index(api_index, symbols):
    queue = [api_index]
    while len(queue) > 0:
        item = queue.pop()
        symbols.append({
            "name": item["name"],
            "displayName": item["displayName"],
            "kind": item["kind"],
            "lib": item["lib"]
        })
        if "nodes" in item:
            queue.extend(item["nodes"])


for key in [OPENUI5, SAPUI5]:
    print("#", key)
    file_name = "./webapp/data/" + key + "/api-index.json"
    if not os.path.exists(file_name):
        api_index = read_api_index(key)
        with open(file_name, "w") as fh:
            fh.write(json.dumps(api_index))
    else:
        with open(file_name) as fh:
            api_index = json.load(fh)
    symbols = []
    for item in api_index["symbols"]:
        recurse_api_index(item, symbols)
        libs = set(item["lib"] for item in symbols)
        for lib in libs:
            file_name = "./webapp/data/" + key + "/" + lib + ".json"
            if not os.path.exists(file_name):
                content = read_library(key, lib)
                with open(file_name, "w") as fh:
                    fh.write(json.dumps(content))
                content = json.loads(content)
            else:
                with open(file_name) as fh:
                    content = json.load(fh)

