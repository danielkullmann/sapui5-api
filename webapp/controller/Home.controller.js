sap.ui.define([
	  "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
	  "../model/formatter"
], function(Controller, JSONModel, formatter) {
	  "use strict";

	  return Controller.extend("danielkullmann.apidocs.controller.Home", {

		    formatter: formatter,

        mBaseUrls: {
            openui5: "https://openui5.hana.ondemand.com/",
            sapui5: "https://sapui5.hana.ondemand.com/"
        },

        mIndex: {
            openui5: {},
            sapui5: {}
        },

        mLibrary: {
            openui5: {},
            sapui5: {}
        },

        // openui5 or sapui5
        sBase: null,

        loadJson: function(sUrl) {
            return fetch(sUrl).then(response => response.json());
        },

        processApiIndex: function(oJson) {
            var aSymbols = [];
            var fnProcess = (aNodes) => {
                for (var oNode of aNodes) {
                    if (oNode.visibility === "public") {
                        aSymbols.push({
                            Name: oNode.name,
                            Library: oNode.lib,
                            Kind: oNode.kind,
                        });
                        if (oNode.nodes) {
                            fnProcess(oNode.nodes);
                        }
                    }
                }
            };
            fnProcess(oJson.symbols);
            return aSymbols;
        },

        processLibrary: function(oJson) {
            var aLibrary = [];
            var fnProcess = (aNodes, sPath) => {
                for (var oSymbol of aNodes) {
                    if (!oSymbol.href) {
                        aLibrary.push(oSymbol);
                    }
                    if (oSymbol.nodes) {
                        fnProcess(oSymbol.nodes, sPath + "." + oSymbol.name);
                        delete oSymbol.nodes;
                    }
                }
            };
            fnProcess(oJson.symbols, "");
            return aLibrary;
        },

        loadApiIndex: function(sKey) {
            var sUrl = "./data/" + sKey + "/";
            sUrl += "api-index.json";
            return this.loadJson(sUrl).then(oJson => {
                this.mIndex[sKey] = this.processApiIndex(oJson);
            });
        },

        loadApiLibrary: function(sKey, sLibrary) {
            if (this.mLibrary[sKey][sLibrary]) {
                return Promise.resolve(this.mLibrary[sKey][sLibrary]);
            } else {
                var sUrl = "./data/" + sKey + "/" + sLibrary + ".json";
                return this.loadJson(sUrl).then(oJson => {
                    var aLibrary = this.processLibrary(oJson);
                    this.mLibrary[sKey][sLibrary] = aLibrary;
                    return aLibrary;
                });
            }
        },

        onSwitch: function() {
            var bOpenUi5 = this.getView().byId("open-or-sap").getState();
            this.sBase = bOpenUi5 ? "openui5" : "sapui5";
            this.oSearchClassModel.setData(this.mIndex[this.sBase]);

        },

        onSubmitSearchClass: function(oEvent) {
            var sClass = oEvent.getSource().getValue();
            var oSymbol = this.mIndex[this.sBase].filter((oItem) => oItem.Name === sClass)[0];
            var sLibrary = oSymbol.Library;
            var pLibrary = this.loadApiLibrary(this.sBase, sLibrary);
            pLibrary.then((aLibrary) => {
                var oItem = aLibrary.filter((oItem) => oItem.name === sClass)[0];
                this.displayClass(oItem);
            });
        },

        onSubmitSearchItem: function(oEvent) {
            var sItem = oEvent.getSource().getValue();
            var oSymbol = this.oClass.events.filter((oItem) => oItem.name === sItem).concat(
                this.oClass.methods.filter((oItem) => oItem.name === sItem))[0];
            console.log(oSymbol);
            this.oClassModel.setProperty("/Description", oSymbol.description);
        },
        displayClass: function(oClass) {
            this.oClass = oClass;
            this.oClassModel.setProperty("/Description", oClass.description);
            this.getView().byId("search-item").focus();
            var aSearchData = [];
            for (var oItem of oClass.events) {
                aSearchData.push({
                    Name: oItem.name
                });
            }
            for (var oItem of oClass.methods) {
                aSearchData.push({
                    Name: oItem.name
                });
            }
            console.log(oClass);
            this.oSearchItemModel.setData(aSearchData);
        },

		    onInit: function () {
            this.oRouter = this.getOwnerComponent().getRouter();
            this.oRouter.getRoute("home").attachPatternMatched(this._onRouteMatched, this);

            this.byId("search-class").setFilterFunction(function (sTerm, oItem) {
				        // A case-insensitive "string contains" style filter
				        return oItem.getText().match(new RegExp(sTerm, "i"));
			      });

            this.oSearchClassModel = new JSONModel([]);
            this.oSearchClassModel.setSizeLimit(1000000);
            this.getView().setModel(this.oSearchClassModel, "search-class");

            this.oSearchItemModel = new JSONModel([]);
            this.oSearchItemModel.setSizeLimit(1000000);
            this.getView().setModel(this.oSearchItemModel, "search-item");

            this.oClassModel = new JSONModel({});
            this.getView().setModel(this.oClassModel, "class");
		    },

        _onRouteMatched: function() {

            var oView = this.getView();
            Promise.allSettled([
                this.loadApiIndex("openui5"),
                this.loadApiIndex("sapui5")
            ]).then( () => {
                this.onSwitch();
            });

        }

	});
});
