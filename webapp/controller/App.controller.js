sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"danielkullmann/apidocs/model/formatter"
], function(Controller, formatter) {
	"use strict";

	return Controller.extend("danielkullmann.apidocs.controller.App", {

		formatter: formatter,

		onInit: function () {

		}
	});
});